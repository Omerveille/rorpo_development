# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/odyssee/src/RORPO/RORPO_src_developement/Image/Nifti/src/nifti1_io.c" "/home/odyssee/src/RORPO/RORPO_src_developement/Image/Nifti/CMakeFiles/Nifti.dir/src/nifti1_io.c.o"
  "/home/odyssee/src/RORPO/RORPO_src_developement/Image/Nifti/src/znzlib.c" "/home/odyssee/src/RORPO/RORPO_src_developement/Image/Nifti/CMakeFiles/Nifti.dir/src/znzlib.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "libRORPO/include"
  "Image/include"
  "Image/Nifti/include"
  "docopt"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
